const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');

const Note = mongoose.model('notes');

module.exports = app => {
	app.get('/api/notes', requireLogin, async (req, res) => {
		const notes = await Note.find({_user: req.user.id});
		res.send(notes);
	});

	app.post('/api/notes', requireLogin, async (req, res) => {
		const { title, content, tags } = req.body;

		const note = new Note({
			title,
			content,
			tags,
			_user: req.user.id,
			dateSent: Date.now()
		});

		try {
	      await note.save();
	      req.user.notes += 1;
	      const user = await req.user.save();
		  res.send(user);
    	} 
    	catch (err) {
      		res.status(422).send(err);
    	}
	});

	app.delete('/api/notes/:id', requireLogin, (req, res) => {
		Note.findOneAndDelete(req.params.id, (err) => {
			if (err) {
				console.log(err);
			} else {
				req.user.notes-=1;
				req.user.save();
				res.send({});
			} 
		})
	});

	app.put('/api/notes/:id', requireLogin, (req, res) => {
		const note = {
			title: req.body.title,
			content: req.body.content,
			tags: req.body.tags,
			dateSent: Date.now()
		}
		Note.findByIdAndUpdate(req.params.id, note, (err) => {
			if (err) {
				console.log(err);
			} else {
				console.log("success")
				res.send({});
			} 
		})
	});

	app.get('/api/notes/search', requireLogin, async (req, res) => {
		let note = await Note.find({_user: req.user.id});
		if (req.query.title) {
			note = await Note.find({_user: req.user.id, title: req.query.title});
		} else {
			note = await Note.find({_user: req.user.id});
		}
		res.send(note);
	});
}
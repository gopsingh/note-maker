const mongoose = require('mongoose');

const { Schema } = mongoose;

const noteSchema = new Schema({
	title: String,
	content: String,
	tags: String,
	dateSent: Date,
	_user: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	}
}); 
mongoose.model('notes', noteSchema);
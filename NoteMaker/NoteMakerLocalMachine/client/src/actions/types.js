export const FETCH_USER = 'FETCH_USER';
export const FETCH_NOTES = 'FETCH_NOTES';
export const SUBMIT_NOTE = 'SUBMIT_NOTE';
export const SUBMIT_START = 'SUBMIT_START';
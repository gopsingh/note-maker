import * as axios from 'axios';
import * as actions from './types';

export const checkUser = (user) => {
	return {
		type: actions.FETCH_USER,
		user: user
	}
};

export const fetchNote = (notes) => {
	return {
		type: actions.FETCH_NOTES,
		notes: notes
	}
};

export const submitStart = () => {
	return {
		type: actions.SUBMIT_START
	}
}

export const successSubmit = () => {
	return {
		type: actions.SUBMIT_NOTE
	}
}

export const fetchUser = () => {
	return dispatch => {
		axios.get('/api/current_user')
			.then(res => {
				dispatch(checkUser(res.data));
			});
	}
}

export const submitNote = (formValues, history) => {
	return dispatch => {
		dispatch(submitStart());
		axios.post('/api/notes', formValues)
			.then(res => {
				history.push('/notes');
				dispatch(successSubmit());
			})
	}
}

export const fetchNotes = () => {
	return dispatch => {
		axios.get('/api/notes')
			.then(res => {
				dispatch(fetchNote(res.data));
			});
	}
}

export const deleteNote = (id, history) => {
	return dispatch => {
		dispatch(submitStart())
		axios.delete('/api/notes/' + id)
			.then((res) => {			
				history.push('/notes');
				dispatch(successSubmit());
			});
	}
}

export const editNote = (formValues, id, history) => {
	return dispatch => {
		dispatch(submitStart())
		axios.put(`/api/notes/${id}`, formValues)
			.then(res => {
				history.push('/notes');
				dispatch(successSubmit());
			})
			.catch(err => console.log(err));
	}
}

export const searchTitle = (title) => {
	return dispatch => {
		axios.get(`/api/notes/search?title=${title}`)
			.then(res => {
				dispatch(fetchNote(res.data));
			});
	}
}
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

class Header extends Component {
	// renderContent() {
	// 	switch(this.props.auth) {
	// 		case null:
	// 			return;
	// 		case false:
	// 			return <NavItem eventKey={1} href="/auth/google">Login with Google</NavItem>;
	// 		default:
	// 			return [
	// 				<Navbar.Text key='1'>Total Notes: {this.props.auth.notes}</Navbar.Text>,
	// 				<NavItem eventKey={1} key='2' href="/api/logout">Logout</NavItem>
	// 			];
	// 	}
	// }

	render() {
		window.$(document).ready(function(){
		    window.$('.sidenav').sidenav();
		});
		let notes = 0;
		if (this.props.auth) {
			notes = this.props.auth.notes;
		}

		return (
			<div>
				<nav>
				    <div className="nav-wrapper">
				      <NavLink to='/notes' className="brand-logo">NoteMaker</NavLink>
				      <a href="#" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
				      <ul className="right hide-on-med-and-down">
				        <li>Total Notes: {notes}</li>
				        <li><a href="/api/logout">Logout</a></li>
				       </ul>
				    </div>
				</nav>

				<ul className="sidenav" id="mobile-demo">
				    <li><a href="#" style={{cursor: 'auto'}}>Total Notes: {notes}</a></li>
				    <li><a href="/api/logout">Logout</a></li>
				</ul>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		auth: state.auth
	}
}

export default connect(mapStateToProps)(Header);
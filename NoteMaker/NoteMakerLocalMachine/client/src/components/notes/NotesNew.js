import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import NotesForm from './NotesForm';
import NotesFormReview from './NotesFormReview';
import '../stylesheets/NotesNew.css';

class NotesNew extends Component {
	state = {
		showFormReview: false
	}

	renderContent() {
		if (this.state.showFormReview) {
			return (
				<NotesFormReview onCancel={() => this.setState({showFormReview: false})} />
			);
		}

		return (
			<NotesForm onNoteSubmit={() => this.setState({showFormReview: true})} />
		);
	}

	render() {
		return (
			<div className='new'>
				{this.renderContent()}
			</div>
		);
	}
}

export default reduxForm({
	form: 'noteForm'
})(NotesNew);
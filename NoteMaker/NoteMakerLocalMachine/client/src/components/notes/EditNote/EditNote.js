import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/index'
import './EditNote.css';
import '../../stylesheets/Spinner.css';

class EditNote extends Component {
	state = {
		title: null,
		content: null,
		tags: null,
		validationErrors: []
	}

	componentDidMount() {
		return this.props.notes.map(note => {
			if (note._id === this.props.match.params.id) {
				this.setState({title: note.title, content: note.content, tags: note.tags});
			}
		});
	}

	onTitleChange = (event) => {
        const title = event.target.value;
        this.setState({ title: title });
	}

	onContentChange = (event) => {
        const content = event.target.value;        
        this.setState({ content: content });
    }


    onTagsChange = (event) => {
        const tags = event.target.value;        
        this.setState({ tags: tags});
	}
	
	handleSubmit = (event) => {
		event.preventDefault();
		const formValues = {
			title: this.state.title,
			content: this.state.content,
			tags: this.state.tags
		}
		console.log(formValues)
		this.props.editNote(formValues, this.props.match.params.id, this.props.history);
	}

	render() {
		let spinner = (
			<div className="edit">
				<div className="row">
    				<form onSubmit={this.handleSubmit}>
      					<div className="row">
        					<div className="input-field col s12">
          						<label style={{position: 'unset'}} htmlFor="title">Title</label>
								<input required type="text" name="title" autoFocus onChange={this.onTitleChange} value={this.state.title}/>
        					</div>
      					</div>
					    <div className="row">
					        <div className="input-field col s12">
					          	<label style={{position: 'unset'}} htmlFor="content">Description</label>
								<input required type="text" name="content" autoFocus onChange={this.onContentChange} value={this.state.content}/>
        					</div>
      					</div>
					    <div className="row">
					        <div className="input-field col s12">
					        	<label style={{position: 'unset'}} htmlFor="tags">Tags</label>
								<input type="text" name="tags" autoFocus onChange={this.onTagsChange} value={this.state.tags}/>
					        </div>
					    </div>
					    <button className="btn waves-effect waves-light" type="submit">Submit</button>
      				</form>
  				</div>
			</div>
		);

		if (this.props.loading) {
			spinner = (
				<div className="preloader-wrapper small active spinner">
			        <div className="spinner-layer spinner-green-only">
			          <div className="circle-clipper left">
			            <div className="circle"></div>
			          </div><div className="gap-patch">
			            <div className="circle"></div>
			          </div><div className="circle-clipper right">
			            <div className="circle"></div>
			          </div>
			        </div>
			    </div>
			);
		}

		return (
			<div style={{marginTop: 10}}>
				{spinner}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		notes: state.note.notes,
		loading: state.note.loadOnSubmit
	};
}

const mapDispatchToProps = dispatch => {
	return {
		editNote: (formValues,id, history) => dispatch(actions.editNote(formValues,id, history))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(EditNote);
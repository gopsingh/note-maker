import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/index';
import EditNote from './EditNote/EditNote';
import { NavLink } from 'react-router-dom';
import '../stylesheets/NoteHandler.css';
import '../stylesheets/Spinner.css';

class NoteHandler extends Component {
	deleteNote(id, history) {
		const confirmation = window.confirm('Do you want to remove note?');
		if (confirmation) {
			this.props.deleteNote(id, history);
		}
	}

	editNote(id) {
		this.props.history.push('/notes/' + id + '/edit');
	}

	findNote() {
		return this.props.notes.map(note => {
			if (note._id === this.props.match.params.id) {
				return (
					<div >
						<ul className="collection">
						    <li className="collection-item avatar list">
						      <h1 className="title heading">Title</h1>
						      <p>{note.title}
						      </p>
						    </li>
						    <li className="collection-item avatar list">
						      <h3 className="title heading">Description</h3>
						      <p>{note.content}
						      </p>
						    </li>
						    <li className="collection-item avatar list">
						      <h3 className="title heading">Tags</h3>
						      <p>{note.tags}
						      </p>
						    </li>
						</ul>
					</div>
				);
			}
		});
	}

	render() {
		const id = this.props.match.params.id;
		const history = this.props.history;
		let spinner = this.findNote();
		if (this.props.loading) {
			spinner = (
				<div className="preloader-wrapper small active spinner">
			        <div className="spinner-layer spinner-green-only">
			          <div className="circle-clipper left">
			            <div className="circle"></div>
			          </div><div className="gap-patch">
			            <div className="circle"></div>
			          </div><div className="circle-clipper right">
			            <div className="circle"></div>
			          </div>
			        </div>
			    </div>
			);
		}

		return (
			<div className="collections">
				{spinner}
				<button className="waves-effect waves-light btn" onClick={() => this.editNote(id)}>Edit</button>
				<button className="waves-effect waves-light btn btnd" onClick={() => this.deleteNote(id, history)}>Delete</button>
			</div>
		);
	}
} 

const mapStateToProps = state => {
	return {
		notes: state.note.notes,
		loading: state.note.loadOnSubmit
	};
}

const mapDispatchToProps = dispatch => {
	return {
		deleteNote: (id, history) => dispatch(actions.deleteNote(id, history))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(NoteHandler);
import React from 'react';
import './stylesheets/Home.css';

const Home = () => {
	return (
		<div>
			<main>
    			<center>
					<div>
						<div className="z-depth-1 grey lighten-4 row home">
							<h1 className="indigo-text" style={{fontSize: 'xx-large'}}>NoteMaker</h1>
							<a href="/auth/facebook"><button className="loginBtn loginBtn--facebook" style={{color: '#FFF'}}>
							Login with Facebook
							</button></a>

							<a href="/auth/google"><button className="loginBtn loginBtn--google">
							  Login with Google
							</button></a>
  						</div>
						</div>
    			</center>
			</main>
		</div>
	);
}

export default Home;
import * as actions from '../actions/types';

const reducer = (state = null, action) => {
	switch (action.type) {
		case actions.FETCH_USER:
			return action.user || false;
		default:
			return state;
	}
}

export default reducer;
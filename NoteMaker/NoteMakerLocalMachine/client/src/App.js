import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import logo from './logo.svg';
import Header from './components/Header';
import { connect } from 'react-redux';
import * as actions from './actions/index'
import Dashboard from './components/Dashboard';
import NotesNew from './components/notes/NotesNew';
import NoteHandler from './components/notes/NoteHandler';
import EditNote from './components/notes/EditNote/EditNote';
import Home from './components/Home';

class App extends Component {
	state = {
		showHeader: false
	}

	componentDidMount() {
		this.props.fetchUser();
	}

  render() {
  	let route = null, routes = null;
  	console.log(this.props.auth)
	if (this.props.auth === false) {
		route = <Home />
	} else {
		route = <Header />
		routes = (
			<Switch>
				<Route exact path='/notes/new' component={NotesNew} />
				<Route exact path='/notes' component={Dashboard} />
				<Route exact path='/notes/:id/edit' component={EditNote} />
				<Route path='/notes/:id' component={NoteHandler} />
			</Switch>
		);
	}

    return (
      <div>
		{route}
		{routes}
      </div>
    );
  }
}

const mapStateToProps = state => {
	return {
		auth: state.auth
	}
}

const mapDispatchToProps = dispatch => {
	return {
		fetchUser: () => dispatch(actions.fetchUser())
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

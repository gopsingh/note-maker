const passport = require('passport');
const mongoose = require('mongoose');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
	done(null, user.id);
});

passport.deserializeUser((id, done) => {
	//var userId = mongoose.Schema.Types.ObjectId(id);
	User.findById(id).then(user => {
		done(null, user);
	});
});

passport.use(new GoogleStrategy({
	clientID: keys.googleClientID,
	clientSecret: keys.googleClientSecret,
	callbackURL: '/auth/google/callback',
	proxy: true
}, (accessToken, refreshToken, profile, done) => {
	// let imageUrl = '';
	// if (profile.photos && profile.photos.length) {
	//     imageUrl = profile.photos[0].value;
	// }
	// var userData =  {
	//     id: profile.id,
	//     displayName: profile.displayName,
	//     image: imageUrl,
	//     email: profile.emails[0].value
	//   };

	User.findOne({userId: profile.id}).then(existingUser => {
		if (existingUser) {
			//we already have a user
			done(null, existingUser);
		} else {
			//create a new user
			new User({userId: profile.id}).save()
				.then(user => done(null, user));
		}
	});
}));
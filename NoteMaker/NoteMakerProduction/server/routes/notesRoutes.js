const mongoose = require('mongoose');

const Note = mongoose.model('notes');

module.exports = app => {
	app.get('/api/notes', async (req, res) => {
		const notes = await Note.find();
		res.send(notes);
	});

	app.post('/api/notes', async (req, res) => {
		const { title, content, tags } = req.body;

		const note = new Note({
			title,
			content,
			tags,
			dateSent: Date.now()
		});

		try {
	    	const not = await note.save();
			res.send(not);
    	} 
    	catch (err) {
      		res.status(422).send(err);
    	}
	});

	app.put('/api/notes/:id', (req, res) => {
		const note = {
			title: req.body.title,
			content: req.body.content,
			tags: req.body.tags,
			dateSent: Date.now()
		}
		Note.findByIdAndUpdate(req.params.id, note, (err) => {
			if (err) {
				console.log(err);
			} else {
				console.log("success")
				res.send({});
			} 
		})
	});

	app.get('/api/notes/search', async (req, res) => {
		let note = await Note.find();
		if (req.query.title) {
			note = await Note.find({title: req.query.title});
		}
		res.send(note);
	});

	app.get('/api/:id', (req, res) => {
		Note.findById(req.params.id, (err, note) => {
			if (err) {
				console.log(err)
			} else {
				res.send(note)
			}
		})
	});
}
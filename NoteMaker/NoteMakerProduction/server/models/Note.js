const mongoose = require('mongoose');

const { Schema } = mongoose;

const noteSchema = new Schema({
	title: String,
	content: String,
	tags: String,
	dateSent: Date
}); 
mongoose.model('notes', noteSchema);
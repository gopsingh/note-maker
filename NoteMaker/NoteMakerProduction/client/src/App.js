import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import logo from './logo.svg';
import Header from './components/Header';
import Dashboard from './components/Dashboard';
import NotesNew from './components/notes/NotesNew';
import NoteHandler from './components/notes/NoteHandler';
import EditNote from './components/notes/EditNote/EditNote';

class App extends Component {
  render() {
  		let route , routes = null;
		route = <Header />
		routes = (
			<Switch>
				<Route exact path='/notemaker/notes/new' component={NotesNew} />
				<Route exact path='/notemaker' component={Dashboard} />
				<Route exact path='/notemaker/notes/:id/edit' component={EditNote} />
				<Route path='/notemaker/notes/:id' component={NoteHandler} />
			</Switch>
		);

    return (
      <div>
		{route}
		{routes}
      </div>
    );
  }
}

export default withRouter(App);

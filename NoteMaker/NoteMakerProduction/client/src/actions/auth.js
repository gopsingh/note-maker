import * as axios from 'axios';
import * as actions from './types';

export const fetchNote = (notes) => {
	return {
		type: actions.FETCH_NOTES,
		notes: notes
	}
};

export const submitStart = () => {
	return {
		type: actions.SUBMIT_START
	}
}

export const successSubmit = () => {
	return {
		type: actions.SUBMIT_NOTE
	}
}

export const submitNote = (formValues, history) => {
	return dispatch => {
		dispatch(submitStart());
		axios.post('https://safe-sea-79589.herokuapp.com/api/notes', formValues)
			.then(res => {
				history.push('/notemaker');
				dispatch(successSubmit());
			})
	}
}

export const fetchNotes = () => {
	return dispatch => {
		axios.get('https://safe-sea-79589.herokuapp.com/api/notes')
			.then(res => {
				dispatch(fetchNote(res.data));
			});
	}
}

export const editNote = (formValues, id, history) => {
	return dispatch => {
		dispatch(submitStart())
		axios.put(`https://safe-sea-79589.herokuapp.com/api/notes/${id}`, formValues)
			.then(res => {
				history.push('/notemaker');
				dispatch(successSubmit());
			})
			.catch(err => console.log(err));
	}
}

export const searchTitle = (title) => {
	return dispatch => {
		axios.get(`https://safe-sea-79589.herokuapp.com/api/notes/search?title=${title}`)
			.then(res => {
				dispatch(fetchNote(res.data));
			});
	}
}
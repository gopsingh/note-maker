import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/index';
import EditNote from './EditNote/EditNote';
import { NavLink } from 'react-router-dom';
import '../stylesheets/NoteHandler.css';
import '../stylesheets/Spinner.css';
import axios from 'axios';

class NoteHandler extends Component {
	state = {
		note: null,
		loading: true
	}

	editNote(id) {
		this.props.history.push('/notemaker/notes/' + id + '/edit');
	}

	componentDidMount() {
		axios.get(`https://safe-sea-79589.herokuapp.com/api/${this.props.match.params.id}`)
			.then(res => {
				this.setState( {note: res.data, loading: false} );
		});
	}

	findNote() {
		return (
			<div >
				<ul className="collection">
				    <li className="collection-item avatar list">
				      <h1 className="title heading">Title</h1>
				      <p>{this.state.note.title}
				      </p>
				    </li>
				    <li className="collection-item avatar list">
				      <h3 className="title heading">Description</h3>
				      <p>{this.state.note.content}
				      </p>
				    </li>
				    <li className="collection-item avatar list">
				      <h3 className="title heading">Tags</h3>
				      <p>{this.state.note.tags}
				      </p>
				    </li>
				</ul>
			</div>
		);
	}

	render() {
		const id = this.props.match.params.id;
		let spinner = (
			<div className="preloader-wrapper small active spinner">
		        <div className="spinner-layer spinner-green-only">
		          <div className="circle-clipper left">
		            <div className="circle"></div>
		          </div><div className="gap-patch">
		            <div className="circle"></div>
		          </div><div className="circle-clipper right">
		            <div className="circle"></div>
		          </div>
		        </div>
		    </div>
		);
		if (!this.state.loading) {
			spinner = this.findNote();
		}

		return (
			<div className="collections">
				{spinner}
				<button className="waves-effect waves-light btn" onClick={() => this.editNote(id)}>Edit</button>
			</div>
		);
	}
} 

export default (NoteHandler);
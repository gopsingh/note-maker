export default [
  { label: 'Title', name: 'title' },
  { label: 'Content', name: 'content' },
  { label: 'Tags', name: 'tags' }
];
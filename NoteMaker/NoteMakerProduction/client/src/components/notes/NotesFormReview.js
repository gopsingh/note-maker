import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import formFields from './formField';
import { withRouter } from 'react-router-dom';
import * as actions from '../../actions/index';
import '../stylesheets/Spinner.css';

const NoteFormReview = (props) => {
  const reviewFields = _.map(formFields, ({ name, label }) => {
    return (
      <div key={name}>
        <label>{label}</label>
        <div>
          {props.formValues[name]}
        </div>
      </div>
    );
  });

  const note = () => {
    props.onSubmitNote(props.formValues, props.history);
  }
  
  let spinner = null;
  if (props.loading) {
    spinner = (
      <div className="preloader-wrapper small active spinner">
        <div className="spinner-layer spinner-green-only">
          <div className="circle-clipper left">
            <div className="circle"></div>
          </div><div className="gap-patch">
            <div className="circle"></div>
          </div><div className="circle-clipper right">
            <div className="circle"></div>
          </div>
        </div>
      </div>
    );
  } else {
    spinner = (<div> 
      <h5>Please confirm your entries</h5>
      {reviewFields}
      <button
        className="yellow darken-3 white-text btn-flat"
        onClick={props.onCancel}>Back
      </button>
      <button
        onClick={note}
        className="green btn-flat right white-text"
      >
        Create Note
      </button>
    </div>);
  }

  return (
    <div> 
      {spinner}
    </div>
  );
};

function mapStateToProps(state) {
  return { 
    formValues: state.form.noteForm.values,
    loading: state.note.loadOnSubmit
    };
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmitNote: (formValues, history) => dispatch( actions.submitNote(formValues, history) )
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NoteFormReview));
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/index';
import '../stylesheets/Spinner.css';

class NoteList extends Component {
	componentDidMount() {
		this.props.fetchNotes();
	}

	renderNotes() {
		return this.props.notes.reverse().map(note => {
				var inDate = new Date(new Date().toLocaleString("en-UK",{timeZone:'Etc/UTC'}));
				var mydate = new Date();
				let date = new Date(note.dateSent);
				var timeDifference = mydate - inDate;
				var differenceInMinutes = Math.floor(timeDifference/(1000*60));
				date.setMinutes(date.getMinutes()+date.getTimezoneOffset()+differenceInMinutes);
				return (
			          <tr style={{cursor: 'pointer'}} key={note._id} onClick={() => this.props.clicked(note._id)}>			      			         
			            <td>{note.title}</td>
			            <td>{note.content.substring(0,10)}...</td>
			            <td>{date.toString().slice(0,24)}</td>
			          </tr>			    
				);
			});	
	}

	render() {
		let noteInfo = null;
		if (this.props.loading) {
			noteInfo = (
				<div className="preloader-wrapper small active spinner">
				    <div className="spinner-layer spinner-green-only">
				      <div className="circle-clipper left">
				        <div className="circle"></div>
				      </div><div className="gap-patch">
				        <div className="circle"></div>
				      </div><div className="circle-clipper right">
				        <div className="circle"></div>
				      </div>
				    </div>
				</div>
			);
		} else {
			noteInfo = this.renderNotes();
		}

		return ( 
			<div>
				<table className='striped'>
			        <thead>
			          <tr>			          	  
			              <th>Title</th>
			              <th>Description</th>
			              <th>Updated Date</th>
			          </tr>
			        </thead>	
			        <tbody>
						{noteInfo}
					</tbody>
				</table>
			</div>
		);
	}	
}

const mapStateToProps = state => {
	return {
		notes: state.note.notes,
		loading: state.note.loading
	};
}

const mapDispatchToProps = dispatch => {
	return {
		fetchNotes: () => dispatch(actions.fetchNotes())
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(NoteList);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/index'
import './EditNote.css';
import '../../stylesheets/Spinner.css';
import axios from 'axios'

class EditNote extends Component {
	state = {
		title: null,
		content: null,
		tags: null,
		validationErrors: [],
		loading: true
	}

	componentDidMount() {
		axios.get(`https://safe-sea-79589.herokuapp.com/api/${this.props.match.params.id}`)
			.then(res => {
				this.setState({title: res.data.title, content: res.data.content, tags: res.data.tags, loading: false});
		});
	}

	onTitleChange = (event) => {
        const title = event.target.value;
        this.setState({ title: title });
	}

	onContentChange = (event) => {
        const content = event.target.value;        
        this.setState({ content: content });
    }


    onTagsChange = (event) => {
        const tags = event.target.value;        
        this.setState({ tags: tags});
	}
	
	handleSubmit = (event) => {
		event.preventDefault();
		const formValues = {
			title: this.state.title,
			content: this.state.content,
			tags: this.state.tags
		}
		this.props.editNote(formValues, this.props.match.params.id, this.props.history);
	}

	render() {
		let spinner = (
			<div className="edit">
				<div className="row">
    				<form onSubmit={this.handleSubmit}>
      					<div className="row">
        					<div className="input-field col s12">
          						<label style={{position: 'unset'}} htmlFor="title">Title</label>
								<input required type="text" name="title" autoFocus onChange={this.onTitleChange} value={this.state.title}/>
        					</div>
      					</div>
					    <div className="row">
					        <div className="input-field col s12">
					          	<label style={{position: 'unset'}} htmlFor="content">Description</label>
								<input required type="text" name="content" autoFocus onChange={this.onContentChange} value={this.state.content}/>
        					</div>
      					</div>
					    <div className="row">
					        <div className="input-field col s12">
					        	<label style={{position: 'unset'}} htmlFor="tags">Tags</label>
								<input type="text" name="tags" autoFocus onChange={this.onTagsChange} value={this.state.tags}/>
					        </div>
					    </div>
					    <button className="btn waves-effect waves-light" type="submit">Submit</button>
      				</form>
  				</div>
			</div>
		);

		if (this.state.loading) {
			spinner = (
				<div className="preloader-wrapper small active spinner">
			        <div className="spinner-layer spinner-green-only">
			          <div className="circle-clipper left">
			            <div className="circle"></div>
			          </div><div className="gap-patch">
			            <div className="circle"></div>
			          </div><div className="circle-clipper right">
			            <div className="circle"></div>
			          </div>
			        </div>
			    </div>
			);
		}

		return (
			<div style={{marginTop: 10}}>
				{spinner}
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		editNote: (formValues,id, history) => dispatch(actions.editNote(formValues,id, history))
	};
}

export default connect(null, mapDispatchToProps)(EditNote);
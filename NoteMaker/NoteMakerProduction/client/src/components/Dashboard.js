import React from 'react';
import { NavLink } from 'react-router-dom';
import NoteList from './notes/NoteList'
import './stylesheets/Dashboard.css';
import  { connect } from 'react-redux';
import * as actions from '../actions/index'

const Dashboard = (props) => {
	const showClick = (id) => {
		props.history.push('/notemaker/notes/' + id);
	}

	const onSearchTitle = (event) => {
		props.searchTitle(event.target.value);
	}

	return (
		<div className="style">
			<div className="row">
      			<div className="col col1">
      				<NavLink to='/notemaker/notes/new'>
						<i className="material-icons link">add</i>
					</NavLink>
				</div>
      			<div className="col s11 col11" style={{padding: 'unset'}}><nav>
			    	<div className="nav-wrapper">
			      		<form>
			        		<div className="input-field">			   
			          			<input id="search" type="search" placeholder="Search note by title" onChange={onSearchTitle} required />
			          			<label className="label-icon"><i className="material-icons">search</i></label>
			          			<i className="material-icons">close</i>
			       			</div>
			      		</form>
			    	</div>
		  		</nav></div>
    		</div>
		  	<NoteList clicked={showClick} />
		</div>
	);
}

const mapDispatchToProps = dipatch => {
	return {
		searchTitle: (title) => dipatch(actions.searchTitle(title))
	};
}

export default connect(null,mapDispatchToProps)(Dashboard);
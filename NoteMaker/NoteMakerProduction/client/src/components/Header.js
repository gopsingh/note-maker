import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import * as actions from '../actions/index';
import axios from 'axios';

class Header extends Component {
	// state = {
	// 	notes: []
	// }

	// componentDidMount() {
	// 	axios.get('https://safe-sea-79589.herokuapp.com/api/notes')
	// 		.then(res => {
	// 			this.setState({notes: res.data});
	// 		})
	// }

	render() {
		window.$(document).ready(function(){
		    window.$('.sidenav').sidenav();
		});

		return (
			<div>
				<nav>
				    <div className="nav-wrapper">
				      <NavLink to='/notemaker' className="brand-logo">NoteMaker</NavLink>
				      <a href="#" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
				      <ul className="right hide-on-med-and-down">
				        <li>Total Notes: {this.props.notes.length}</li>
				       </ul>
				    </div>
				</nav>

				<ul className="sidenav" id="mobile-demo">
				    <li><a href="#" style={{cursor: 'auto'}}>Total Notes: {this.props.notes.length}</a></li>
				</ul>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		notes: state.note.notes
	}
}

export default connect(mapStateToProps)(Header);
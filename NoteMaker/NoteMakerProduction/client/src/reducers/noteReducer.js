import * as actions from '../actions/types';

const initialState = {
	loading: true,
	notes: [],
	loadOnSubmit: false
}

const reducer = (state = initialState, action) => {
	switch(action.type) {
		case actions.FETCH_NOTES:
			return {
				...state,
				loading: false,
				notes: action.notes
			};
		case actions.SUBMIT_START:
			return {
				...state,
				loadOnSubmit: true
			};
		case actions.SUBMIT_NOTE:
			return {
				...state,
				loadOnSubmit: false
			};
		default:
			return state;
	}
}

export default reducer;